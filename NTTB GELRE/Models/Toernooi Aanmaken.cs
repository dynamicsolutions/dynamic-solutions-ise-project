﻿namespace NTTB_GELRE.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Web;

    public class ToernooiAanmaken
    {
        [Required]
        public string Locatienaam { get; set; }

        [Required]
        public int TellingID { get; set; }

        [Required]
        public string Toernooinaam { get; set; }

        [Required]
        public System.DateTime Begindatum { get; set; }

        [Required]
        public System.DateTime Einddatum { get; set; }

        [Required]
        public System.DateTime Inschrijfdatum { get; set; }

        public Nullable<decimal> InschrijfgeldEnkel { get; set; }
        public Nullable<decimal> InschrijfgeldDubbel { get; set; }

        [Required]
        public string Organisatie { get; set; }

        public string Bondsvertegenwoordiger { get; set; }

        [Required]
        public int GewonnenGames { get; set; }

        public Nullable<int> Tijdlimiet { get; set; }

        [Required]
        public Nullable<int> Tafels { get; set; }

        [Required]
        public string Categorie { get; set; }

        public Boolean Niveaus { get; set; }

        [Required]
        public string Toernooivorm1 { get; set; }

        public string Toernooivorm2 { get; set; }
        public string Toernooivorm3 { get; set; }
        public string Toernooivorm4 { get; set; }


    }
}
