﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NTTB_GELRE.Models;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Entity.Core.EntityClient;

namespace NTTB_GELRE.Controllers
{
    public class DeelnameController : Controller
    {
        private NTTBGELREEntities db = new NTTBGELREEntities();

        // GET: /Deelname/
        public ActionResult Index()
        {

            var openstaandeToernooien = (from t in db.Toernooi
                                         join l in db.Locatie on t.Locatienaam equals l.Locatienaam
<<<<<<< HEAD
                                         where System.DateTime.Now < t.BeginInschrijving
=======
                                         where System.DateTime.Now < t.EindInschrijving
>>>>>>> 7bc8efd4806e9220f5f9d95b78851a42dcfb1e89
                                         orderby t.Begindatum
                                         select t);

            return View(openstaandeToernooien.ToList());
        }


        // GET: /Deelname/Inschrijven
        public ActionResult Inschrijven(int id)
        {

            inschrijvenViewbags(id);

            string toernooisoort = (from ts in db.Toernooischema
                                    join c in db.Categorie on ts.CategorieID equals c.CategorieID
                                    where ts.ToernooiID == id
                                    select c.Leeftijdsgroep).FirstOrDefault();

            if (toernooisoort.Contains("Familie") )
            {
                return View("InschrijvenFamilie");
            }
            else
            {
                var spelerID = Convert.ToInt32(Session["spelerID"]);

                if (Session["spelerID"] != null)
                {

                    ViewBag.alIngeschreven = (from d in db.Deelnemer
                                              where d.ToernooiID == id
                                                && d.SpelerID == spelerID
                                              select d).Count();
                }

                var maxInschrijvingen = (from t in db.Toernooi
                                         where t.ToernooiID == id
                                         select t.MaxDeelnemers).FirstOrDefault();

                var huidigAantalInschrijvingen = (from d in db.Deelnemer
                                                  where d.ToernooiID == id
                                                  select d).Count();

                string leeftijdscategorieToernooi = (from tc in db.Toernooicategorie
                                                     join c in db.Categorie on tc.CategorieID equals c.CategorieID
                                                     where tc.ToernooiID == id
                                                        && c.Leeftijdsgroep == "Senioren"
                                                     select c.Leeftijdsgroep).Distinct().ToList().FirstOrDefault();


                //Leeftijdscategorie Speler
                var geboortedatumSpeler = (from s in db.Speler
                                           where s.SpelerID == spelerID
                                           select s.Geboortedatum).FirstOrDefault();

                string leeftijdscategorieSpeler;

                if (GetAge(geboortedatumSpeler) > 18)
                {
                    leeftijdscategorieSpeler = "Senioren";
                }
                else
                {
                    leeftijdscategorieSpeler = "Junioren";
                }

                if (leeftijdscategorieToernooi == null)
                {
                    leeftijdscategorieToernooi = "Junioren";
                }

                ViewBag.LeeftijdSpeler = leeftijdscategorieSpeler;
                ViewBag.LeeftijdToernooi = leeftijdscategorieToernooi;

                // Check of leeftijdscategorieen overeenkomen
                if (leeftijdscategorieSpeler != leeftijdscategorieToernooi.ToString())
                {
                    ViewBag.Leeftijdscategorie = false;
                }
                else
                {
                    ViewBag.Leeftijdscategorie = true;
                }


                // Check max inschrijvingen
                if (huidigAantalInschrijvingen < maxInschrijvingen)
                {
                    ViewBag.maxInschrijvingenBereikt = false;
                }
                else
                {
                    ViewBag.maxInschrijvingenBereikt = true;
                }


                return View("InschrijvenLid");
            }

            
        }


        // POST: /Deelname/InschrijvenFamilie
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult InschrijvenFamilie(InschrijvingFamilie InschrijvingFamilie)
        {
            if (ModelState.IsValid)
            {

                // Stored procedure
                NTTBGELREEntities ctx = new NTTBGELREEntities();
                System.Data.Common.DbConnection dbConn = ctx.Database.Connection;
                System.Data.SqlClient.SqlConnection sc = (System.Data.SqlClient.SqlConnection)dbConn;
                string adoConnStr = sc.ConnectionString;

                using (SqlConnection connection = new SqlConnection(adoConnStr))
                {
                    connection.Open();
                    // Stored procedure naam = spInschrijvingSpelerZonderAccount
                    SqlCommand cmd = new SqlCommand("spInschrijvingSpelerZonderAccount", connection);
                    cmd.CommandType = CommandType.StoredProcedure;

                    // Paramaters voor de stored procedure
                    cmd.Parameters.AddWithValue("@voornaam", InschrijvingFamilie.Voornaam);
                    cmd.Parameters.AddWithValue("@tussenvoegsel", InschrijvingFamilie.Tussenvoegsel);
                    cmd.Parameters.AddWithValue("@achternaam", InschrijvingFamilie.Achternaam);
                    cmd.Parameters.AddWithValue("@geboortedatum", InschrijvingFamilie.Geboortedatum);
                    cmd.Parameters.AddWithValue("@geslacht", InschrijvingFamilie.Geslacht);
                    cmd.Parameters.AddWithValue("@mailadres", InschrijvingFamilie.Mailadres);
                    cmd.Parameters.AddWithValue("@telefoonnummer", InschrijvingFamilie.Telefoonnummer);
                    cmd.Parameters.AddWithValue("@enkel", InschrijvingFamilie.Enkel);
                    cmd.Parameters.AddWithValue("@dubbel", InschrijvingFamilie.Dubbel);
                    cmd.Parameters.AddWithValue("@toernooiID", InschrijvingFamilie.ToernooiID);
                    cmd.Parameters.AddWithValue("@partner", null);
                    cmd.ExecuteNonQuery();
                }

                // Inschrijving succesvol
                return View("InschrijvingVoltooid");
            }

            // Er ging iets mis
            inschrijvenViewbags(InschrijvingFamilie.ToernooiID);
            ViewBag.Error = "Er ging iets mis, probeer het later nog eens";
            return RedirectToAction("Index");
        }


        // POST: /Deelname/InschrijvenLid
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult InschrijvenLid(InschrijvingLid InschrijvingLid)
        {
            if (ModelState.IsValid)
            {

                // Stored procedure
                NTTBGELREEntities ctx = new NTTBGELREEntities();
                System.Data.Common.DbConnection dbConn = ctx.Database.Connection;
                System.Data.SqlClient.SqlConnection sc = (System.Data.SqlClient.SqlConnection)dbConn;
                string adoConnStr = sc.ConnectionString;

                using (SqlConnection connection = new SqlConnection(adoConnStr))
                {
                    connection.Open();
                    // Stored procedure naam = spInschrijvingSpelerMetAccount
                    SqlCommand cmd = new SqlCommand("spInschrijvingSpelerMetAccount", connection);
                    cmd.CommandType = CommandType.StoredProcedure;

                    // Paramaters voor de stored procedure
                    cmd.Parameters.AddWithValue("@spelerID", InschrijvingLid.SpelerID);
                    cmd.Parameters.AddWithValue("@toernooiID", InschrijvingLid.ToernooiID);
                    cmd.Parameters.AddWithValue("@enkel", InschrijvingLid.Enkel);
                    cmd.Parameters.AddWithValue("@dubbel", InschrijvingLid.Dubbel);
                    cmd.Parameters.AddWithValue("@partner", null);
                    cmd.ExecuteNonQuery();
                }

                // Inschrijving succesvol
                return View("InschrijvingVoltooid");
            }

            // Er ging iets mis
            inschrijvenViewbags(InschrijvingLid.ToernooiID);
            ViewBag.Error = "Er ging iets mis, probeer het later nog eens";
            return RedirectToAction("Index");
        }


        // GET: /Deelname/Details/5
        //public ActionResult Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Speler speler = db.Speler.Find(id);
        //    if (speler == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(speler);
        //}


        // GET: /Deelname/Edit/5
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Speler speler = db.Speler.Find(id);
        //    if (speler == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(speler);
        //}


        //// POST: /Deelname/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include="SpelerID,Voornaam,Tussenvoegsel,Achternaam,Bondsnummer,Geboortedatum,Geslacht,Mailadres,Telefoonnummer,Licentie,Punten")] Speler speler)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(speler).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    return View(speler);
        //}


        //// GET: /Deelname/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Speler speler = db.Speler.Find(id);
        //    if (speler == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(speler);
        //}

        //// POST: /Deelname/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    Speler speler = db.Speler.Find(id);
        //    db.Speler.Remove(speler);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }



        private void inschrijvenViewbags(int toernooiID) 
        {
            ViewBag.toernooiID = toernooiID;

            ViewBag.toernooinaam = (from t in db.Toernooi
                                    where t.ToernooiID.Equals(toernooiID)
                                    select t.Toernooinaam).FirstOrDefault();

            // Categorieen combobox waardes
            var categorieenQry = (from ts in db.Toernooischema
                                  join c in db.Categorie on ts.CategorieID equals c.CategorieID
                                  where ts.ToernooiID.Equals(toernooiID) &&
                                        ts.SchemaDeel.Equals(1)
                                  select new SelectListItem { Value = c.CategorieID.ToString(), Text = c.Leeftijdsgroep + " -- " + c.EnkelDubbel + " -- " + c.Geslacht + " -- " + c.Niveau });

            ViewBag.categorie = new SelectList(categorieenQry, "Value", "Text");

            // Geslacht combobox waardes
            var geslachten = new List<SelectListItem>()
            {
                new SelectListItem {Value = "M", Text = "Man"},
                new SelectListItem {Value = "V", Text = "Vrouw"}
            };

            ViewBag.geslacht = new SelectList(geslachten, "Value", "Text");

            // Licentie combobox waardes
            var licenties = new List<SelectListItem>()
            {
                new SelectListItem {Value = "A", Text = "A"},
                new SelectListItem {Value = "B", Text = "B"},
                new SelectListItem {Value = "C", Text = "C"},
                new SelectListItem {Value = "D", Text = "D"},
                new SelectListItem {Value = "E", Text = "E"},
                new SelectListItem {Value = "F", Text = "F"},
            };

            ViewBag.licentie = new SelectList(licenties, "Value", "Text");

        }


        private Int32 GetAge(DateTime dateOfBirth)
        {
            var today = DateTime.Today;

            var a = (today.Year * 100 + today.Month) * 100 + today.Day;
            var b = (dateOfBirth.Year * 100 + dateOfBirth.Month) * 100 + dateOfBirth.Day;

            return (a - b) / 10000;
        }



    }
}
