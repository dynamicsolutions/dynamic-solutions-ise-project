﻿namespace NTTB_GELRE.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System;
    using System.Linq;
    using System.Web;

    public class SchemaToernooivorm
    {
        public int ToernooiID {get; set;}
        public int CategorieID {get; set;}
        public int SchemaDeel {get; set;}
        public string Toernooivorm { get; set; }
        public int AantalPoules { get; set; }
        public int AantalRondes { get; set; }
        public int AantalDoor { get; set; }
    }

    public class SchemaDetails
    {
        public long ToernooiID {get; set;}
        public int CategorieID {get; set;}
        public int SchemaDeel {get; set;}
        public long ID { get; set; }
        public string Geslacht { get; set; }
        public string Leeftijdsgroep { get; set; }
        public string Subcategorie { get; set; }
        public string EnkelDubbel { get; set; }
        public string Niveau { get; set; }
        public int Deelnemers { get; set; }
        public string Toernooivorm1 { get; set; }
        public string Toernooivorm2 { get; set; }
        public string Toernooivorm3 { get; set; }
        public Nullable<int> Wedstrijden { get; set; }
        public Nullable<int> AantalPoules1 { get; set; }
        public Nullable<int> AantalPoules2 { get; set; }
        public Nullable<int> AantalPoules3 { get; set; }
        public Nullable<int> AantalDoor1 { get; set; }
        public Nullable<int> AantalDoor2 { get; set; }
        public Nullable<int> AantalDoor3 { get; set; }
        public Nullable<int> AantalRondes1 { get; set; }
        public Nullable<int> AantalRondes2 { get; set; }
        public Nullable<int> AantalRondes3 { get; set; }

        public virtual Toernooicategorie Toernooicategorie { get; set; }
    }


    
}
