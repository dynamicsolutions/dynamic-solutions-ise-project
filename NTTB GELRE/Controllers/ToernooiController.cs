﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NTTB_GELRE.Models;
using System.Data.SqlClient;
using System.Data.Entity.Core.EntityClient;
using System.Configuration;
using System.Web.SessionState;

namespace NTTB_GELRE.Controllers
{
    public class ToernooiController : Controller
    {
        private NTTBGELREEntities db = new NTTBGELREEntities();


        public ToernooiController(){

                   
         
        }
        // GET: Toernooi
        public ActionResult Index()
        {
            
            var toernoois = db.Toernooi.Include(t => t.Locatie).Include(t => t.Puntentelling);
            return View(toernoois.ToList());
        }

      
        // GET: Toernooi/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Toernooi toernooi = db.Toernooi.Find(id);
            if (toernooi == null)
            {
                return HttpNotFound();
            }
            return View(toernooi);
        }

        // GET: Toernooi/Create
        public ActionResult Create()
        {
            ViewBag.addCar = false;
            //ViewData["categorie"] = db.Categorie.ToList();
            ViewBag.Locatienaam = new SelectList(db.Locatie, "Locatienaam", "Locatienaam");

            
            //ViewBag.Locatienaam = new SelectList(db.Locatie, "Locatienaam", "Locatienaam");

            var leeftijdsgroepen = db.Categorie.Select(c => c.Leeftijdsgroep).Distinct();
            ViewBag.Categorie = new SelectList(leeftijdsgroepen);
       
            ViewBag.TellingID = new SelectList(db.Puntentelling, "TellingID", "Tot");

            List<SelectListItem> puntentellingen = new List<SelectListItem>();
            puntentellingen.Add(new SelectListItem { Text = "1", Value = "1" });
            puntentellingen.Add(new SelectListItem { Text = "3", Value = "3" });
            puntentellingen.Add(new SelectListItem { Text = "5", Value = "5" });
            puntentellingen.Add(new SelectListItem { Text = "7", Value = "7" });
            puntentellingen.Add(new SelectListItem { Text = "9", Value = "9" });
            puntentellingen.Add(new SelectListItem { Text = "11", Value = "11" });

            ViewBag.GewonnenGames = puntentellingen;

            var toernooivormenLst = new List<string>();

            var toernooivormenQry = (from t in db.Toernooivorm
                                     select t.Toernooivorm1);

            toernooivormenLst.AddRange(toernooivormenQry.Distinct());
            ViewBag.toernooivormen = new SelectList(toernooivormenLst);

            return View();
        }

        // POST: Toernooi/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ToernooiAanmaken toernooi)
        {
            if (ModelState.IsValid)
            {

                // Stored procedure
                NTTBGELREEntities ctx = new NTTBGELREEntities();
                System.Data.Common.DbConnection dbConn = ctx.Database.Connection;
                System.Data.SqlClient.SqlConnection sc = (System.Data.SqlClient.SqlConnection)dbConn;
                string adoConnStr = sc.ConnectionString;

                using (SqlConnection connection = new SqlConnection(adoConnStr))
                {
                    connection.Open();
                    // Stored procedure naam = inschrijving
                    SqlCommand cmd = new SqlCommand("spSchemasAanmaken", connection);
                    cmd.CommandType = CommandType.StoredProcedure;

                    // Paramaters voor de stored procedure
                    cmd.Parameters.AddWithValue("@Locatienaam", toernooi.Locatienaam);
                    cmd.Parameters.AddWithValue("@TellingID", toernooi.TellingID);
                    cmd.Parameters.AddWithValue("@Toernooinaam", toernooi.Toernooinaam);
                    cmd.Parameters.AddWithValue("@Begindatum", toernooi.Begindatum);
                    cmd.Parameters.AddWithValue("@Einddatum", toernooi.Einddatum);
                    cmd.Parameters.AddWithValue("@Inschrijfdatum", toernooi.Inschrijfdatum);
                    cmd.Parameters.AddWithValue("@InschrijfGeldEnkel", toernooi.InschrijfgeldEnkel);
                    cmd.Parameters.AddWithValue("@InschrijfGeldDubbel", toernooi.InschrijfgeldDubbel);
                    cmd.Parameters.AddWithValue("@Organisatie", toernooi.Organisatie);
                    cmd.Parameters.AddWithValue("@Bondsvertegenwoordiger", toernooi.Bondsvertegenwoordiger);
                    cmd.Parameters.AddWithValue("@GewonnenGames", toernooi.GewonnenGames);
                    cmd.Parameters.AddWithValue("@Tijdlimiet", toernooi.Tijdlimiet);
                    cmd.Parameters.AddWithValue("@Tafels", toernooi.Tafels);
                    cmd.Parameters.AddWithValue("@Categorie", toernooi.Categorie);
                    cmd.Parameters.AddWithValue("@Niveaus", toernooi.Niveaus);
                    cmd.Parameters.AddWithValue("@Toernooivorm1", toernooi.Toernooivorm1);
                    cmd.Parameters.AddWithValue("@Toernooivorm2", toernooi.Toernooivorm2);
                    cmd.Parameters.AddWithValue("@Toernooivorm3", toernooi.Toernooivorm3);
                    cmd.Parameters.AddWithValue("@Toernooivorm4", toernooi.Toernooivorm4);
                    
                    
                    cmd.ExecuteNonQuery();
                }

                return RedirectToAction("Index","Home");
            }
            ViewBag.Locatienaam = new SelectList(db.Locatie, "Locatienaam", "Locatienaam");


            //ViewBag.Locatienaam = new SelectList(db.Locatie, "Locatienaam", "Locatienaam");

            var leeftijdsgroepen = db.Categorie.Select(c => c.Leeftijdsgroep).Distinct();
            ViewBag.Categorie = new SelectList(leeftijdsgroepen);
            
            ViewBag.TellingID = new SelectList(db.Puntentelling, "TellingID", "Tot");
            List<SelectListItem> puntentellingen = new List<SelectListItem>();
            puntentellingen.Add(new SelectListItem { Text = "1", Value = "1" });
            puntentellingen.Add(new SelectListItem { Text = "3", Value = "3" });
            puntentellingen.Add(new SelectListItem { Text = "5", Value = "5" });
            puntentellingen.Add(new SelectListItem { Text = "7", Value = "7" });
            puntentellingen.Add(new SelectListItem { Text = "9", Value = "9" });
            puntentellingen.Add(new SelectListItem { Text = "11", Value = "11" });

            ViewBag.GewonnenGames = puntentellingen;

            var toernooivormenLst = new List<string>();

            var toernooivormenQry = (from t in db.Toernooivorm
                                     select t.Toernooivorm1);

            toernooivormenLst.AddRange(toernooivormenQry.Distinct());
            ViewBag.toernooivormen = new SelectList(toernooivormenLst);

            return View(toernooi);
        }

        // GET: Toernooi/CreateStepTwo
        public ActionResult CreateStepTwo(int toernooiID, string leeftijdsgroep)
        {
            var Categorieen = from c in db.Categorie
                              where c.Leeftijdsgroep == leeftijdsgroep
                              select c;
            return View(Categorieen.ToList());
        }

        // GET: Toernooi/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Toernooi toernooi = db.Toernooi.Find(id);
            if (toernooi == null)
            {
                return HttpNotFound();
            }
            ViewBag.Locatienaam = new SelectList(db.Locatie, "Locatienaam", "Adres", toernooi.Locatienaam);
            ViewBag.TellingID = new SelectList(db.Puntentelling, "TellingID", "TellingID", toernooi.TellingID);
            return View(toernooi);
        }

        // POST: Toernooi/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ToernooiID,Locatienaam,TellingID,Toernooinaam,Begindatum,Einddatum,Inschrijfdatum,InschrijfgeldEnkel,InschrijfgeldDubbel,Organisatie,Bondsvertegenwoordiger,GewonnenGames,Tijdlimiet,Tafels")] Toernooi toernooi)
        {
            if (ModelState.IsValid)
            {
                db.Entry(toernooi).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
  
            ViewBag.Locatienaam = new SelectList(db.Locatie, "Locatienaam", "Adres", toernooi.Locatienaam);
            ViewBag.TellingID = new SelectList(db.Puntentelling, "TellingID", "TellingID", toernooi.TellingID);
            return View(toernooi);
        }

        // GET: Toernooi/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Toernooi toernooi = db.Toernooi.Find(id);
            if (toernooi == null)
            {
                return HttpNotFound();
            }
            return View(toernooi);
        }

        // POST: Toernooi/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Toernooi toernooi = db.Toernooi.Find(id);
            db.Toernooi.Remove(toernooi);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
