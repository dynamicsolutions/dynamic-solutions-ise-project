﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NTTB_GELRE.Models;
using System.Data.SqlClient;

namespace NTTB_GELRE.Controllers
{
    public class SpelergegevensController : Controller
    {
        private NTTBGELREEntities db = new NTTBGELREEntities();

        // GET: Spelergegevens
        public ActionResult Index()
        {
            var SessionSpelerID = Session["spelerID"];

            ViewBag.SpelerID = SessionSpelerID;

            return View();
        }


        // GET: Spelergegevens/ZoekSpeler
        public ActionResult ZoekSpeler(string mailadres, string voornaam, string tussenvoegsel, string achternaam, DateTime? geboortedatum, int bondsnummer = 0)
        {
            var RolWerknemer = "Administratie";

            if (RolWerknemer == "Administratie")
            {

                var spelers = (from s in db.Speler
                               select s);

                // Filter by bondsnummer
                if (bondsnummer > 0)
                {
                    spelers = spelers.Where(v => v.Bondsnummer == (bondsnummer));
                }

                // Filter by mailadres
                if (!String.IsNullOrEmpty(mailadres))
                {
                    spelers = spelers.Where(n => n.Mailadres.Contains(mailadres));
                }

                // Filter by voornaam
                if (!String.IsNullOrEmpty(voornaam))
                {
                    spelers = spelers.Where(n => n.Voornaam.Contains(voornaam));
                }

                // Filter by tussenvoegsel
                if (!String.IsNullOrEmpty(tussenvoegsel))
                {
                    spelers = spelers.Where(n => n.Tussenvoegsel.Contains(tussenvoegsel));
                }

                // Filter by achternaam
                if (!String.IsNullOrEmpty(achternaam))
                {
                    spelers = spelers.Where(n => n.Achternaam.Contains(achternaam));
                }

                // Filter by geboortedatum
                if (geboortedatum.HasValue)
                {
                    spelers = spelers.Where(v => v.Geboortedatum == (geboortedatum));
                }

                return View(spelers);

            }
            else
            {
                return View();
            }

        }


        // GET: Spelergegevens/EditPersoonsgegevens/5
        public ActionResult EditPersoonsgegevens(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Speler speler = db.Speler.Find(id);
            if (speler == null)
            {
                return HttpNotFound();
            }

            // Geslacht combobox waardes
            var geslachten = new List<SelectListItem>()
            {
                new SelectListItem {Value = "M", Text = "Man"},
                new SelectListItem {Value = "V", Text = "Vrouw"}
            };

            ViewBag.geslacht = new SelectList(geslachten, "Value", "Text");

            // Licentie combobox waardes
            var licenties = new List<SelectListItem>()
            {
                new SelectListItem {Value = "A", Text = "A"},
                new SelectListItem {Value = "B", Text = "B"},
                new SelectListItem {Value = "C", Text = "C"},
                new SelectListItem {Value = "D", Text = "D"},
                new SelectListItem {Value = "E", Text = "E"},
                new SelectListItem {Value = "F", Text = "F"},
            };

            ViewBag.licentie = new SelectList(licenties, "Value", "Text");

            return View(speler);
        }


        // POST: Spelergegevens/EditPersoonsgegevens/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditPersoonsgegevens([Bind(Exclude = "Wachtwoord,WachtwoordSalt")] Speler speler)
        {
            if (ModelState.IsValid)
            {

                // Stored procedure
                NTTBGELREEntities ctx = new NTTBGELREEntities();
                System.Data.Common.DbConnection dbConn = ctx.Database.Connection;
                System.Data.SqlClient.SqlConnection sc = (System.Data.SqlClient.SqlConnection)dbConn;
                string adoConnStr = sc.ConnectionString;

                using (SqlConnection connection = new SqlConnection(adoConnStr))
                {
                    connection.Open();
                    // Stored procedure naam = spInschrijvingSpelerZonderAccount
                    SqlCommand cmd = new SqlCommand("spEditSpelergegevens", connection);
                    cmd.CommandType = CommandType.StoredProcedure;

                    // Paramaters voor de stored procedure
                    cmd.Parameters.AddWithValue("@spelerID", speler.SpelerID);
                    cmd.Parameters.AddWithValue("@voornaam", speler.Voornaam);
                    cmd.Parameters.AddWithValue("@tussenvoegsel", speler.Tussenvoegsel);
                    cmd.Parameters.AddWithValue("@achternaam", speler.Achternaam);
                    cmd.Parameters.AddWithValue("@bondsnummer", speler.Bondsnummer);
                    cmd.Parameters.AddWithValue("@geboortedatum", speler.Geboortedatum);
                    cmd.Parameters.AddWithValue("@geslacht", speler.Geslacht);
                    cmd.Parameters.AddWithValue("@telefoonnummer", speler.Telefoonnummer);
                    cmd.Parameters.AddWithValue("@mailadres", speler.Mailadres);
                    cmd.Parameters.AddWithValue("@licentie", speler.Licentie);
                    cmd.Parameters.AddWithValue("@punten", speler.Punten);

                    cmd.ExecuteNonQuery();
                }

                // Inschrijving succesvol
                ViewBag.SuccessMessage = "Uw gegevens zijn succesvol aangepast";
                return View("Succes");
            }

            // Er ging iets mis
            ViewBag.Error = "Er ging iets mis, probeer het later nog eens";
            return RedirectToAction("Index", new { id = speler.SpelerID });

        }


        // GET: Spelergegevens/Inschrijvingen/5
        public ActionResult Inschrijvingen(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Speler speler = db.Speler.Find(id);
            if (speler == null)
            {
                return HttpNotFound();
            }

            var inschrijvingen = (from d in db.Deelnemer
                                  join c in db.Categorie on d.CategorieID equals c.CategorieID
                                  join t in db.Toernooi on d.ToernooiID equals t.ToernooiID
                                  where d.SpelerID == id
                                  orderby t.Begindatum
                                  select new Inschrijvingen { ToernooiID = d.ToernooiID, CategorieID = c.CategorieID, SpelerID = d.SpelerID, Toernooinaam = t.Toernooinaam, Begindatum = t.Begindatum, Einddatum = t.Einddatum, EnkelDubbel = c.EnkelDubbel, Licentie = c.Niveau, EindInschrijving = t.EindInschrijving });
            
            return View(inschrijvingen);
        }


        // GET: Spelergegevens/EditInschrijving/5
        public ActionResult EditInschrijving(int? toernooiID, int? categorieID, int? spelerID)
        {
            if (toernooiID == null || categorieID == null || spelerID == null )
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var inschrijving = (from d in db.Deelnemer
                                join c in db.Categorie on d.CategorieID equals c.CategorieID
                                join t in db.Toernooi on d.ToernooiID equals t.ToernooiID
                                where d.SpelerID == spelerID
                                   && d.CategorieID == categorieID
                                   && d.ToernooiID == toernooiID
                                select new Inschrijvingen { ToernooiID = d.ToernooiID, CategorieID = c.CategorieID, SpelerID = d.SpelerID, Toernooinaam = t.Toernooinaam, Begindatum = t.Begindatum, Einddatum = t.Einddatum, EnkelDubbel = c.EnkelDubbel, Licentie = c.Niveau, EindInschrijving = t.EindInschrijving }).SingleOrDefault();

            return View(inschrijving);
        }


        // POST: Spelergegevens/EditInschrijving/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditInschrijving(Inschrijvingen inschrijving)
        {
            if (ModelState.IsValid)
            {

                // Stored procedure
                NTTBGELREEntities ctx = new NTTBGELREEntities();
                System.Data.Common.DbConnection dbConn = ctx.Database.Connection;
                System.Data.SqlClient.SqlConnection sc = (System.Data.SqlClient.SqlConnection)dbConn;
                string adoConnStr = sc.ConnectionString;

                using (SqlConnection connection = new SqlConnection(adoConnStr))
                {
                    connection.Open();
                    // Stored procedure naam = spEditInschrijving
                    SqlCommand cmd = new SqlCommand("spEditInschrijving", connection);
                    cmd.CommandType = CommandType.StoredProcedure;

                    // Paramaters voor de stored procedure
                    cmd.Parameters.AddWithValue("@toernooiID", inschrijving.ToernooiID);
                    cmd.Parameters.AddWithValue("@categorieID", inschrijving.CategorieID);
                    cmd.Parameters.AddWithValue("@spelerID", inschrijving.SpelerID);
                    cmd.Parameters.AddWithValue("@licentie", inschrijving.Licentie);

                    cmd.ExecuteNonQuery();
                }

                // Inschrijving succesvol
                ViewBag.SuccessMessage = "De inschrijving is aangepast";
                return View("Succes");
            }

            // Er ging iets mis
            ViewBag.Error = "Er ging iets mis, probeer het later nog eens";
            return RedirectToAction("Index", new { id = inschrijving.SpelerID });
        }


        


        // GET: Spelergegevens/DeleteInschrijving/5
        public ActionResult DeleteInschrijving(int? toernooiID, int? categorieID, int? spelerID)
        {
            if (toernooiID == null || categorieID == null || spelerID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var inschrijving = (from d in db.Deelnemer
                                join c in db.Categorie on d.CategorieID equals c.CategorieID
                                join t in db.Toernooi on d.ToernooiID equals t.ToernooiID
                                where d.SpelerID == spelerID
                                   && d.CategorieID == categorieID
                                   && d.ToernooiID == toernooiID
                                select new Inschrijvingen { ToernooiID = d.ToernooiID, CategorieID = d.CategorieID, SpelerID = d.SpelerID, Toernooinaam = t.Toernooinaam, Begindatum = t.Begindatum, Einddatum = t.Einddatum, EnkelDubbel = c.EnkelDubbel, Licentie = c.Niveau, EindInschrijving = t.EindInschrijving }).SingleOrDefault();

            return View(inschrijving);
        }

        // POST: Spelergegevens/DeleteInschrijving/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteInschrijving(Inschrijvingen inschrijving)
        {
            if (ModelState.IsValid)
            {

                // Stored procedure
                NTTBGELREEntities ctx = new NTTBGELREEntities();
                System.Data.Common.DbConnection dbConn = ctx.Database.Connection;
                System.Data.SqlClient.SqlConnection sc = (System.Data.SqlClient.SqlConnection)dbConn;
                string adoConnStr = sc.ConnectionString;

                using (SqlConnection connection = new SqlConnection(adoConnStr))
                {
                    connection.Open();
                    // Stored procedure naam = spDeleteInschrijving
                    SqlCommand cmd = new SqlCommand("spDeleteInschrijving", connection);
                    cmd.CommandType = CommandType.StoredProcedure;

                    // Paramaters voor de stored procedure
                    cmd.Parameters.AddWithValue("@toernooiID", inschrijving.ToernooiID);
                    cmd.Parameters.AddWithValue("@categorieID", inschrijving.CategorieID);
                    cmd.Parameters.AddWithValue("@spelerID", inschrijving.SpelerID);

                    cmd.ExecuteNonQuery();
                }

                // Inschrijving succesvol
                ViewBag.SuccessMessage = "De inschrijving is verwijdert";
                return View("Succes");
            }

            // Er ging iets mis
            ViewBag.Error = "Er ging iets mis, probeer het later nog eens";
            return RedirectToAction("Index", new { id = inschrijving.SpelerID });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
