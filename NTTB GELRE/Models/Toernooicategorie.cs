//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NTTB_GELRE.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Toernooicategorie
    {
        public Toernooicategorie()
        {
            this.Deelnemer = new HashSet<Deelnemer>();
            this.Toernooischema = new HashSet<Toernooischema>();
            this.Wedstrijd = new HashSet<Wedstrijd>();
        }
    
        public long ToernooiID { get; set; }
        public int CategorieID { get; set; }
        public Nullable<int> AantalWedstrijden { get; set; }
    
        public virtual Categorie Categorie { get; set; }
        public virtual ICollection<Deelnemer> Deelnemer { get; set; }
        public virtual Toernooi Toernooi { get; set; }
        public virtual ICollection<Toernooischema> Toernooischema { get; set; }
        public virtual ICollection<Wedstrijd> Wedstrijd { get; set; }
    }
}
