﻿namespace NTTB_GELRE.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System;
    using System.Linq;
    using System.Web;

    public class InschrijvingLid
    {

        public int SpelerID { get; set; }

        public int ToernooiID { get; set; }

        public bool Enkel { get; set; }

        public bool Dubbel { get; set; }

        public Nullable<int> Partner { get; set; }
    }

    
}
