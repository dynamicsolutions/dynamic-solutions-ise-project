﻿$(document).ready(function () {
    $(".datepicker").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true
    });

});

var toernooivormnr = 0;
var maxToernooivormen = 4;

// Selected a toernooivormen
$('#toernooivormen').change(function () {
    if (toernooivormnr < maxToernooivormen) {
        toernooivormnr++;
        // Place selected value into textfield
        // ID (hidden)
        $('#Toernooivorm' + toernooivormnr).val($('#toernooivormen option:selected').val());
        // Actual text for the user
        $('#toernooivorm' + toernooivormnr + 'text').text($('#toernooivormen option:selected').text());
        // Show/hide delete buttons
        $("#button" + toernooivormnr).show();
        $("#button" + (toernooivormnr - 1)).hide();

        

            if ($('#toernooivormen option:selected').text() == "Knock-out") {
                $("#toernooivormen option:contains('Knock-out')").attr('disabled', 'disabled');
                $("#toernooivormen option:contains('Poule')").attr('disabled', 'disabled');
                $("#toernooivormen option:contains('Zwitsers')").attr('disabled', 'disabled');

                $("#toernooivormen option:contains('Verliezersronde')").removeAttr('disabled');
            }

            if ($('#toernooivormen option:selected').text() == "Poule") {
                $("#toernooivormen option:contains('Zwitsers')").attr('disabled', 'disabled');

                $("#toernooivormen option:contains('Verliezersronde')").removeAttr('disabled');
            }

            if ($('#toernooivormen option:selected').text() == "Zwitsers") {
                $("#toernooivormen option:contains('Knock-out')").attr('disabled', 'disabled');
                $("#toernooivormen option:contains('Poule')").attr('disabled', 'disabled');
                $("#toernooivormen option:contains('Zwitsers')").attr('disabled', 'disabled');
                $("#toernooivormen option:contains('Verliezersronde')").attr('disabled', 'disabled');
            }

            if ($('#toernooivormen option:selected').text() == "Verliezersronde") {
                $("#toernooivormen option:contains('Knock-out')").attr('disabled', 'disabled');
                $("#toernooivormen option:contains('Poule')").attr('disabled', 'disabled');
                $("#toernooivormen option:contains('Zwitsers')").attr('disabled', 'disabled');
                $("#toernooivormen option:contains('Verliezersronde')").attr('disabled', 'disabled');
            }

            $("#toernooivormen").get(0).selectedIndex = 0;

    }
    else {
        $("#toernooivormen").get(0).selectedIndex = 0;
    }
})

// Delete button 1 pressed
$("#button1").click(function () {
    deletetoernooivormen();
})

// Delete button 2 pressed
$("#button2").click(function () {
    deletetoernooivormen();
})

// Delete button 3 pressed
$("#button3").click(function () {
    deletetoernooivormen();
})

// Delete button 4 pressed
$("#button4").click(function () {
    deletetoernooivormen();
})


// Delete a toernooivormen function
function deletetoernooivormen() {

    if ($('#toernooivorm' + toernooivormnr + 'text').text() == "Knock-out") {
        $("#toernooivormen option:contains('Knock-out')").removeAttr('disabled');
        $("#toernooivormen option:contains('Poule')").removeAttr('disabled');
        $("#toernooivormen option:contains('Zwitsers')").removeAttr('disabled');
    }
    if ($('#toernooivorm' + toernooivormnr + 'text').text() == "Zwitsers") {
        $("#toernooivormen option:contains('Knock-out')").removeAttr('disabled');
        $("#toernooivormen option:contains('Poule')").removeAttr('disabled');
        $("#toernooivormen option:contains('Zwitsers')").removeAttr('disabled');
        $("#toernooivormen option:contains('Verliezersronde')").removeAttr('disabled');
    }
    if ($('#toernooivorm' + toernooivormnr + 'text').text() == "Verliezersronde") {
        $("#toernooivormen option:contains('Verliezersronde')").removeAttr('disabled');
        if ($('#toernooivorm' + toernooivormnr - 1 + 'text').text() == "Poule") {
            $("#toernooivormen option:contains('Knock-out')").removeAttr('disabled');
            $("#toernooivormen option:contains('Poule')").removeAttr('disabled');
        }
    }


    // Remove the values from textfield
    $('#Toernooivorm' + toernooivormnr).val("");
    $('#toernooivorm' + toernooivormnr + 'text').text("");
    // Show/hide delete buttons
    $("#button" + toernooivormnr).hide();

    if (toernooivormnr > 1) {
        $("#button" + (toernooivormnr - 1)).show();
    }
    toernooivormnr--;

    // Sort the selectlist
    $("#toernooivormen").html($('#toernooivormen option').sort(function (x, y) {
        return $(x).text() < $(y).text() ? -1 : 1;
    }))

    $("#toernooivormen").get(0).selectedIndex = 0;
    e.preventDefault();
}


$(document).ready(function () {
    $(".datepicker").datepicker({ dateformat: 'yy-mm-dd' });
});

$('#Categorie').change(function () {
    ($(this).val() == 'Junioren') ? $('#checkbox').show() : $('#checkbox').hide();
});

$('#toernooivormen').ready(function () {
    $("#toernooivormen option:contains('Verliezersronde')").attr('disabled', 'disabled');
});


//var toernooivormnr = 1;

//$('#toernooivormen').change(function () {
//    //Place selected value into textbox
//    //ID (hidden)
//    if ($('#toernooivormen option:selected').text() != "") {
//        $('#Toernooivorm' + toernooivormnr).val($('#toernooivormen option:selected').val());
//        //Actual text for the user
//        $('#toernooivorm' + toernooivormnr + 'text').text($('#toernooivormen option:selected').text());
//        toernooivormnr++;
//    }

//    if ($('#toernooivormen option:selected').text() == "Knock-out") {
//        $("#toernooivormen option:contains('Knock-out')").attr('disabled', 'disabled');
//        $("#toernooivormen option:contains('Poule')").attr('disabled', 'disabled');
//        $("#toernooivormen option:contains('Zwitsers')").attr('disabled', 'disabled');

//        $("#toernooivormen option:contains('Verliezersronde')").removeAttr('disabled');
//    }

//    if ($('#toernooivormen option:selected').text() == "Poule") {
//        $("#toernooivormen option:contains('Zwitsers')").attr('disabled', 'disabled');

//        $("#toernooivormen option:contains('Verliezersronde')").removeAttr('disabled');
//    }

//    if ($('#toernooivormen option:selected').text() == "Zwitsers") {
//        $("#toernooivormen option:contains('Knock-out')").attr('disabled', 'disabled');
//        $("#toernooivormen option:contains('Poule')").attr('disabled', 'disabled');
//        $("#toernooivormen option:contains('Zwitsers')").attr('disabled', 'disabled');
//        $("#toernooivormen option:contains('Verliezersronde')").attr('disabled', 'disabled');
//    }

//    if ($('#toernooivormen option:selected').text() == "Verliezersronde") {
//        $("#toernooivormen option:contains('Knock-out')").attr('disabled', 'disabled');
//        $("#toernooivormen option:contains('Poule')").attr('disabled', 'disabled');
//        $("#toernooivormen option:contains('Zwitsers')").attr('disabled', 'disabled');
//        $("#toernooivormen option:contains('Verliezersronde')").attr('disabled', 'disabled');
//    }
//    $("#toernooivormen option[value='']").attr("selected", "selected");
//}
//);