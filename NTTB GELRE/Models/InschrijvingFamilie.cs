﻿namespace NTTB_GELRE.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System;
    using System.Linq;
    using System.Web;

    public class InschrijvingFamilie
    {

        [Display(Name = "Voornaam*")]
        [Required(ErrorMessage = "Voornaam is vereist")]
        [StringLength(50, ErrorMessage = "Maximaal 50 karakters toegestaan")]
        public string Voornaam { get; set; }

        [StringLength(10, ErrorMessage = "Maximaal 10 karakters toegestaan")]
        public string Tussenvoegsel { get; set; }

        [Display(Name = "Achternaam*")]
        [Required(ErrorMessage = "Achternaam is vereist")]
        [StringLength(50, ErrorMessage = "Maximaal 50 karakters toegestaan")]
        public string Achternaam { get; set; }

        [Display(Name = "Geboortedatum*")]
        [Required(ErrorMessage = "Geboortedatum is vereist")]
        public System.DateTime Geboortedatum { get; set; }

        [Display(Name = "Geslacht*")]
        [Required(ErrorMessage = "Geslacht is vereist")]
        public string Geslacht { get; set; }

        [Display(Name = "E-mail adres")]
        [Required(ErrorMessage = "E-mail adres is vereist")]
        [EmailAddress(ErrorMessage = "Ongeldig e-mail adres")]
        [StringLength(50, ErrorMessage = "Maximaal 50 karakters toegestaan")]
        public string Mailadres { get; set; }

        [Required(ErrorMessage = "Telefoonnummer is vereist")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Alleen cijfers toegestaan")]
        [StringLength(20, ErrorMessage = "Maximaal 20 cijfers toegestaan")]
        public string Telefoonnummer { get; set; }

        public int ToernooiID { get; set; }

        public bool Enkel { get; set; }

        public bool Dubbel { get; set; }

        public string Partner { get; set; }

    }

    
}
