﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Mvc;
using NTTB_GELRE.Models;
using System.Data.SqlClient;
using System.Data.Entity.Core.EntityClient;
using System.Configuration;
using System.Web.SessionState;
using System.Diagnostics;

namespace NTTB_GELRE.Controllers
{
    public class GameScoreController : Controller
    {
        NTTBGELREEntities db = new NTTBGELREEntities();
        // GET: GameScore
        public ActionResult Index()
        {
            var date = System.DateTime.Now;
            String userEmail = Convert.ToString(Session["Werknemer"]);
            var user = db.Werknemer.FirstOrDefault(u => u.Mailadres == userEmail);

     
            var view = new List<Wedstrijd>();
            foreach (var toernooi in user.Toernooi)
            {
                var a = db.Wedstrijd.Where(o => o.ToernooiID.Equals(toernooi.ToernooiID)).ToList();
                foreach (var t in a)
                {
                    view.Add(new Wedstrijd()
                    {
                        Tafelnummer = t.Tafelnummer,
                        Begintijd = t.Begintijd,
                        Speler1 = t.Speler1,
                        Speler2 = t.Speler2,
                        WedstrijdID = t.WedstrijdID,
                        CategorieID = t.CategorieID,
                        ToernooiID = t.ToernooiID
                    });
                }
            }          
           
            ViewData["Wedstrijden2"] = view;

            return View();
        }

        public ActionResult Opgave(Wedstrijd wedstrijd)
        {
            int? opgave = wedstrijd.Opgave;
            if (opgave != null)
            {
                System.Data.Common.DbConnection dbConn = db.Database.Connection;
                System.Data.SqlClient.SqlConnection sc = (System.Data.SqlClient.SqlConnection)dbConn;
                string adoConnStr = sc.ConnectionString;

                using (SqlConnection connection = new SqlConnection(adoConnStr))
                {
                    connection.Open();
                    // Stored procedure naam = inschrijving
                    SqlCommand cmd = new SqlCommand("sp_GameWinnaar", connection);
                    cmd.CommandType = CommandType.StoredProcedure;

                    // Paramaters voor de stored procedure
                    cmd.Parameters.AddWithValue("@CategorieID", wedstrijd.CategorieID);
                    cmd.Parameters.AddWithValue("@ToernooiID", wedstrijd.ToernooiID);
                    cmd.Parameters.AddWithValue("@WedstrijdID", wedstrijd.WedstrijdID);
                    cmd.Parameters.AddWithValue("@Opgave", opgave);

                    cmd.ExecuteNonQuery();
                }
            }

            return View();
        }

        [HttpGet]
        public ActionResult Score(int? id, int? idCat, int tID)
        {
            var query = db.Wedstrijd.First(pv => pv.WedstrijdID == (id) & pv.CategorieID == (idCat) & pv.ToernooiID == (tID));

            ViewBag.Winnaar = false;
            if (!String.IsNullOrEmpty(Convert.ToString(query.Winnaar)))
            {
                ViewBag.Winnaar = true;
            }
            var game = (from g in db.Gamescores
                        where g.WedstrijdID == id & g.CategorieID == idCat & g.ToernooiID == tID 
                        select new
                        {
                            ToernooiID = g.ToernooiID,
                            CategorieID = g.CategorieID,
                      
                            WedstrijdID = g.WedstrijdID,
                            Punten1 = g.Punten1,
                            Punten2 = g.Punten2,
                            Game = g.Game

                        });

            var view = new List<Gamescores>();
            foreach (var g in game)
            {
                view.Add(new Gamescores()
                {

                    ToernooiID = g.ToernooiID,
                    CategorieID = g.CategorieID,
     
                    WedstrijdID = g.WedstrijdID,
                    Punten1 = g.Punten1,
                    Punten2 = g.Punten2,
                    Game = g.Game


                });
            }
            ViewData["Game"] = view;

            return View(query);
        }

        [HttpPost]
        public ActionResult Score(Gamescores gameScore)
        {
            ViewBag.Winnaar = false;
            if (ModelState.IsValid)
            {

                // Stored procedure

                System.Data.Common.DbConnection dbConn = db.Database.Connection;
                System.Data.SqlClient.SqlConnection sc = (System.Data.SqlClient.SqlConnection)dbConn;
                string adoConnStr = sc.ConnectionString;
                var toernooi = db.Toernooi.Find(gameScore.ToernooiID);


                using (SqlConnection connection = new SqlConnection(adoConnStr))
                {
                    connection.Open();
                    // Stored procedure naam = inschrijving
                    SqlCommand cmd = new SqlCommand("sp_GameScore", connection);
                    cmd.CommandType = CommandType.StoredProcedure;

                    // Paramaters voor de stored procedure
                    cmd.Parameters.AddWithValue("@CategorieID", gameScore.CategorieID);
                    cmd.Parameters.AddWithValue("@ToernooiID", gameScore.ToernooiID);
                    cmd.Parameters.AddWithValue("@WedstrijdID", gameScore.WedstrijdID);
                    cmd.Parameters.AddWithValue("@Punten1", gameScore.Punten1);
                    cmd.Parameters.AddWithValue("@Punten2", gameScore.Punten2);
                    cmd.Parameters.AddWithValue("@set_", toernooi.GewonnenGames);
                    cmd.Parameters.AddWithValue("@punttelling_", toernooi.Puntentelling.Tot);
                    cmd.ExecuteNonQuery();
                }
            }
            var query = db.Wedstrijd.First(pv => pv.WedstrijdID == (gameScore.WedstrijdID) & pv.CategorieID == (gameScore.CategorieID) & pv.ToernooiID == (gameScore.ToernooiID) );
            if (!String.IsNullOrEmpty(Convert.ToString(query.Winnaar)))
            {
                ViewBag.Winnaar = true;
            }
            var game = (from g in db.Gamescores
                        where g.WedstrijdID == gameScore.WedstrijdID & g.CategorieID == gameScore.CategorieID & g.ToernooiID == gameScore.ToernooiID
                        select new
                        {
                            ToernooiID = g.ToernooiID,
                            CategorieID = g.CategorieID,
                            WedstrijdID = g.WedstrijdID,
                            Punten1 = g.Punten1,
                            Punten2 = g.Punten2,
                            Game = g.Game

                        });

            var view = new List<Gamescores>();
            foreach (var g in game)
            {
                view.Add(new Gamescores()
                {

                    ToernooiID = g.ToernooiID,
                    CategorieID = g.CategorieID,
                    WedstrijdID = g.WedstrijdID,
                    Punten1 = g.Punten1,
                    Punten2 = g.Punten2,
                    Game = g.Game


                });
            }
            ViewData["Game"] = view;
            return View(query);
        }
    }
}