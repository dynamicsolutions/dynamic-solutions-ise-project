﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace NTTB_GELRE.Models
{
    public class WerknemerUserModel
    {
        // Account gegevens
        [Required]
        [EmailAddress]
        [StringLength(50)]
        [Display(Name="Email adres")]
        public String Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(20, MinimumLength=6)]
        [Display(Name = "Wachtwoord")]
        public String Wachtwoord { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(20, MinimumLength = 6)]
        [Display(Name = "Herhaal")]
        public String Herhaal { get; set; }

        // Algemene gegevens
        [Required]
        [Display(Name = "Voornaam")]
        public String Voornaam { get; set; }

        [Display(Name = "Tussenvoegsel")]
        public String Tussenvoegsel { get; set; }

        [Required]
        [Display(Name = "Achternaam")]
        public String Achternaam { get; set; }

        [Display(Name = "Telefoon")]
        public String Telefoon { get; set; }

        [Display(Name = "Rol")]
        public String Rol { get; set; }
    }
}