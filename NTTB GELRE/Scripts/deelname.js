$(document).ready(function () {
    $(".datepicker").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        yearRange: '-150:+0'
    });

});

var catnr = 0;
var maxCategorieen = 2;

// Selected a categorie
$('#categorie').change(function () {
    if (catnr < maxCategorieen) {
        catnr++;
        // Place selected value into textfield
        // ID (hidden)
        $('#Categorie' + catnr).val($('#categorie option:selected').val());
        // Actual text for the user
        $('#cat' + catnr + 'text').text($('#categorie option:selected').text());
        // Show/hide delete buttons
        $("#button" + catnr).show();
        $("#button" + (catnr - 1)).hide();
        // Remove selected option from selectlist
        $("#categorie option[value='" + $('#categorie').val() + "']").remove();
    }
    else {
        $("#categorie").get(0).selectedIndex = 0;
    }
})

// Delete button 1 pressed
$("#button1").click(function () {
    deleteCategorie();
})

// Delete button 2 pressed
$("#button2").click(function () {
    deleteCategorie();
})


// Delete a categorie function
function deleteCategorie() {
    // Add deleted value back to select list
    $("#categorie").append('<option value="' + $('#Categorie' + catnr).val() + '">' + $('#cat' + catnr + 'text').text() + '</option>');
    // Remove the values from textfield
    $('#Categorie' + catnr).val("");
    $('#cat' + catnr + 'text').text("");
    // Show/hide delete buttons
    $("#button" + catnr).hide();

    if (catnr > 1) {
        $("#button" + (catnr - 1)).show();
    }
    catnr--;

    // Sort the selectlist
    $("#categorie").html($('#categorie option').sort(function (x, y) {
        return $(x).text() < $(y).text() ? -1 : 1;
    }))
    $("#categorie").get(0).selectedIndex = 0;
    e.preventDefault();
}