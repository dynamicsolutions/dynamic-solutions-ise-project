﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NTTB_GELRE.Models;
using System.Diagnostics;

namespace NTTB_GELRE.Controllers
{
    public class CategorieController : Controller
    {
        private NTTBGELREEntities db = new NTTBGELREEntities();

        public ActionResult AddCatToDeelschema(long? id)
        {
            Debug.WriteLine(id);
            return RedirectToAction("categorieList");
          
        }
      
        // GET: Categorie
        public ActionResult Index(long? id)
        {
   
            return View(db.Categorie.ToList());
        }

         [HttpGet]
        public ActionResult categorieList(long? id)
        {
            ViewBag.ToernooiID = id;
            ViewBag.Toernooivorm = new SelectList(db.Toernooivorm, "Toernooivorm1", "Toernooivorm1");
            //var query = (from c in db.Categorie
            //             join ts in db.Toernooischema on c.CategorieID equals ts.CategorieID                          
            //             where  ts.ToernooiID == 1
            //             select new { c.Leeftijdsgroep,c.Subcategorie, c.EnkelDubbel, c.Geslacht,c.Niveau });

            //var cat = new List<Categorie>();
            //foreach (var t in query)
            //{
            //    cat.Add(new Categorie()
            //    {
            //        Leeftijdsgroep = t.Leeftijdsgroep,
            //        Subcategorie = t.Subcategorie,
            //        EnkelDubbel = t.EnkelDubbel,
            //        Geslacht = t.Geslacht,
            //        Niveau = t.Niveau
            //    });
            //}
            return View(db.Categorie.ToList());
        }

        // GET: Categorie/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Categorie categorie = db.Categorie.Find(id);
            if (categorie == null)
            {
                return HttpNotFound();
            }
            return View(categorie);
        }
     


        // GET: Categorie/Create
        public ActionResult Create( )
        {

            return View();
        }

        // POST: Categorie/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CategorieID,Leeftijdsgroep,Subcategorie,EnkelDubbel,Geslacht,Niveau")] Categorie categorie)
        {
            if (ModelState.IsValid)
            {
                db.Categorie.Add(categorie);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(categorie);
        }

        // GET: Categorie/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Categorie categorie = db.Categorie.Find(id);
            if (categorie == null)
            {
                return HttpNotFound();
            }
            return View(categorie);
        }

        // POST: Categorie/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CategorieID,Leeftijdsgroep,Subcategorie,EnkelDubbel,Geslacht,Niveau")] Categorie categorie)
        {
            if (ModelState.IsValid)
            {
                db.Entry(categorie).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(categorie);
        }

        // GET: Categorie/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Categorie categorie = db.Categorie.Find(id);
            if (categorie == null)
            {
                return HttpNotFound();
            }
            return View(categorie);
        }

        // POST: Categorie/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Categorie categorie = db.Categorie.Find(id);
            db.Categorie.Remove(categorie);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
