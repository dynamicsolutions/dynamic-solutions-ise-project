﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NTTB_GELRE.Startup))]
namespace NTTB_GELRE
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
