﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace NTTB_GELRE.Models
{
    public class UserModel
    {
        [Required]
        [EmailAddress]
        [StringLength(50)]
        [Display(Name="Email adres")]
        public String Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(20, MinimumLength=6)]
        [Display(Name = "Wachtwoord")]
        public String Wachtwoord { get; set; }

        public Boolean RememberMe { get; set; }
        
    }
}