//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NTTB_GELRE.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Rollen
    {
        public Rollen()
        {
            this.Werknemer = new HashSet<Werknemer>();
            this.Bevoegdheden = new HashSet<Bevoegdheden>();
        }
    
        public string Rol { get; set; }
    
        public virtual ICollection<Werknemer> Werknemer { get; set; }
        public virtual ICollection<Bevoegdheden> Bevoegdheden { get; set; }
    }
}
