﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NTTB_GELRE.Models;
<<<<<<< HEAD
using System.Data.SqlClient;
using System.Data.Entity.Core.EntityClient;
using System.Configuration;
=======
>>>>>>> 7bc8efd4806e9220f5f9d95b78851a42dcfb1e89

namespace NTTB_GELRE.Controllers
{
    public class ToernooischemaController : Controller
    {
        private NTTBGELREEntities db = new NTTBGELREEntities();

        // GET: Toernooischema
        public ActionResult Index(int id)
<<<<<<< HEAD
        {
            //var toernooischema = (from ts in db.Toernooischema
            //                      join d in db.Deelnemer on new {ts.ToernooiID, ts.CategorieID, ts.SchemaDeel } equals new { d.ToernooiID, d.CategorieID, d.SchemaDeel}
            //                      join c in db.Categorie on ts.CategorieID equals c.CategorieID
            //                      where ts.SchemaDeel.Equals(id)
            //                      group c by ts into g
            //                      let 
            //                      select ts);
            //                      select new ToernooischemaGegevens { ts = pvv.});

            var toernooischema = (from C in db.Categorie
                                 join TS in db.Toernooischema
                                       on new { C.CategorieID, SchemaDeel = 1 }
                                   equals new { TS.CategorieID, TS.SchemaDeel } into TS_join
                                 from TS in TS_join.DefaultIfEmpty()
                                 join TS2 in db.Toernooischema
                                       on new { C.CategorieID, TS.ToernooiID, SchemaDeel = 2 }
                                   equals new { TS2.CategorieID, TS2.ToernooiID, TS2.SchemaDeel } into TS2_join
                                 from TS2 in TS2_join.DefaultIfEmpty()
                                 join TS3 in db.Toernooischema
                                       on new { C.CategorieID, TS.ToernooiID, SchemaDeel = 3 }
                                   equals new { TS3.CategorieID, TS3.ToernooiID, TS3.SchemaDeel } into TS3_join
                                 from TS3 in TS3_join.DefaultIfEmpty()
                                 join D in db.Deelnemer
                                       on new { TS.ToernooiID, TS.CategorieID }
                                   equals new { D.ToernooiID, D.CategorieID }
                                join TC in db.Toernooicategorie
                                       on new { TS.ToernooiID, TS.CategorieID }
                                   equals new { TC.ToernooiID, TC.CategorieID}
                                 where
                                   TS.SchemaDeel == 1 &&
                                   (TS2.SchemaDeel == 2 ||
                                   TS2.SchemaDeel == null) &&
                                   (TS3.SchemaDeel == 3 ||
                                   TS3.SchemaDeel == null) &&
                                   TS.ToernooiID == id
                                 group new { TS, C, TS2, TS3 } by new
                                 {
                                     TS.ToernooiID,
                                     TS.CategorieID,
                                     C.Leeftijdsgroep,
                                     C.Geslacht,
                                     C.Subcategorie,
                                     C.EnkelDubbel,
                                     C.Niveau,
                                     TS.Toernooivorm,
                                     Column1 = TS2.Toernooivorm,
                                     Column2 = TS3.Toernooivorm,
                                     TS.AantalPoules,
                                     TS.AantalRondes,
                                     TS.AantalDoor,
                                     Column3 = TS2.AantalPoules,
                                     Column4 = TS2.AantalRondes,
                                     Column5 = TS2.AantalDoor,
                                     Column6 = TS3.AantalPoules,
                                     Column7 = TS3.AantalRondes,
                                     Column8 = TS3.AantalDoor,
                                     Column9 = TC.AantalWedstrijden
                                 } into g
                                 select new SchemaDetails
                                 {
                                     ToernooiID = g.Key.ToernooiID,
                                     CategorieID = (int)g.Key.CategorieID,
                                     Deelnemers = g.Count(),
                                     Leeftijdsgroep = g.Key.Leeftijdsgroep,
                                     Geslacht = g.Key.Geslacht,
                                     Subcategorie = g.Key.Subcategorie,
                                     EnkelDubbel = g.Key.EnkelDubbel,
                                     Niveau = g.Key.Niveau,
                                     Toernooivorm1 = g.Key.Toernooivorm,
                                     AantalPoules1 = (int?)g.Key.AantalPoules,
                                     AantalDoor1 = (int?)g.Key.AantalDoor,
                                     AantalRondes1 = (int?)g.Key.AantalRondes,
                                     AantalPoules2 = (int?)g.Key.Column3,
                                     AantalDoor2 = (int?)g.Key.Column5,
                                     AantalRondes2 = (int?)g.Key.Column4,
                                     AantalPoules3 = (int?)g.Key.Column6,
                                     AantalDoor3 = (int?)g.Key.Column7,
                                     AantalRondes3 = (int?)g.Key.Column8,
                                     Wedstrijden = (int?)g.Key.Column9,
                                     Toernooivorm2 = g.Key.Column1,
                                     Toernooivorm3 = g.Key.Column2,
                                 });

            
            IEnumerable<Toernooischema> toernooischemas = db.Toernooischema.Where(t => (t.ToernooiID == id));

            var toernooivormenLst = new List<string>();
            var toernooivormenQry = (from t in db.Toernooivorm
                                     select t.Toernooivorm1);

            toernooivormenLst.AddRange(toernooivormenQry.Distinct());
            ViewBag.toernooivormen = new SelectList(toernooivormenLst);

            ViewBag.ToernooiSchema1 = toernooischema.ToList();
            ViewData["ToernooiID"] = id;
            var maximum = (from Toernooi in db.Toernooi
                                              select new
                                              {
                                                  Aantal = (
                                                    (from Tafel in db.Tafel
                                                     where
                                                      Tafel.ToernooiID == id
                                                     select new
                                                     {
                                                         Tafel
                                                     }).Count() *
                                                    (from Tijden in db.Tijden
                                                     where
                                                      Tijden.ToernooiID == id
                                                     select new
                                                     {
                                                         Tijden
                                                     }).Count())
                                              }).First();
            ViewData["MaximumWedstrijden"] = maximum.Aantal;

            var totaalwedstrijden = (from Toernooicategorie in
                                         (from Toernooicategorie in db.Toernooicategorie
                                          where
                                            Toernooicategorie.ToernooiID == id 
                                          select new
                                          {
                                              Toernooicategorie.AantalWedstrijden,
                                              Dummy = "x"
                                          })
                                     group Toernooicategorie by new { Toernooicategorie.Dummy } into g
                                     select new
                                     {
                                         Aantal = g.Sum(p => p.AantalWedstrijden)
                                     }).First();

            ViewData["TotaalWedstrijden"] = totaalwedstrijden.Aantal;

            return View();
            //return View(toernooischema.ToList());
        }

        [HttpPost]
        public String Inplannen(int id)
        {
            NTTBGELREEntities ctx = new NTTBGELREEntities();
            System.Data.Common.DbConnection dbConn = ctx.Database.Connection;
            System.Data.SqlClient.SqlConnection sc = (System.Data.SqlClient.SqlConnection)dbConn;
            string adoConnStr = sc.ConnectionString;

            using (SqlConnection connection = new SqlConnection(adoConnStr))
            {
                connection.Open();
                // Stored procedure naam = inschrijving
                SqlCommand cmd = new SqlCommand("spWedstrijdenInplannen", connection);
                cmd.CommandType = CommandType.StoredProcedure;

                // Paramaters voor de stored procedure
                cmd.Parameters.AddWithValue("@ToernooiID", id);

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException e)
                {
                    string errorMessage = e.Message;
                    int errorCode = e.ErrorCode;
                    return errorMessage;
                }

            }
            return "Wedstrijden ingepland";
        }

        [HttpPost]
        public String AlgemeneToernooivorm(int toernooiid, string toernooivorm1, string toernooivorm2, string toernooivorm3)
        {
            if (toernooivorm2 == "") { toernooivorm2 = null; };
            if (toernooivorm3 == "") { toernooivorm3 = null; };

            NTTBGELREEntities ctx = new NTTBGELREEntities();
            System.Data.Common.DbConnection dbConn = ctx.Database.Connection;
            System.Data.SqlClient.SqlConnection sc = (System.Data.SqlClient.SqlConnection)dbConn;
            string adoConnStr = sc.ConnectionString;

            using (SqlConnection connection = new SqlConnection(adoConnStr))
            {
                connection.Open();
                // Stored procedure naam = inschrijving
                SqlCommand cmd = new SqlCommand("spAlgemeneToernooivorm", connection);
                cmd.CommandType = CommandType.StoredProcedure;

                // Paramaters voor de stored procedure
                cmd.Parameters.AddWithValue("@ToernooiID", toernooiid);
                cmd.Parameters.AddWithValue("@Toernooivorm1", toernooivorm1);
                cmd.Parameters.AddWithValue("@Toernooivorm2", toernooivorm2);
                cmd.Parameters.AddWithValue("@Toernooivorm3", toernooivorm3);

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException e)
                {
                    string errorMessage = e.Message;
                    int errorCode = e.ErrorCode;
                    return errorMessage;
                }

            }
            return "De toernooivorm is ingesteld";
        }

        [HttpPost]
        public String InstellenStandaard(int toernooiid, int poulestandaard, int doorstandaard)
        {

            NTTBGELREEntities ctx = new NTTBGELREEntities();
            System.Data.Common.DbConnection dbConn = ctx.Database.Connection;
            System.Data.SqlClient.SqlConnection sc = (System.Data.SqlClient.SqlConnection)dbConn;
            string adoConnStr = sc.ConnectionString;

            using (SqlConnection connection = new SqlConnection(adoConnStr))
            {
                connection.Open();
                // Stored procedure naam = spSchemasInstellen
                SqlCommand cmd = new SqlCommand("spSchemasInstellen", connection);
                cmd.CommandType = CommandType.StoredProcedure;

                // Paramaters voor de stored procedure
                cmd.Parameters.AddWithValue("@ToernooiID", toernooiid);
                cmd.Parameters.AddWithValue("@PouleStandaard", poulestandaard);
                cmd.Parameters.AddWithValue("@DoorStandaard", doorstandaard);

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException e)
                {
                    string errorMessage = e.Message;
                    int errorCode = e.ErrorCode;
                    return errorMessage;
                }

            }
            return "Poules hebben nu een maximale grootte van " + poulestandaard + " spelers, er gaan " + doorstandaard + " spelers per poule door";
        }

        [HttpPost]
        public String WedstrijdenInplannen(int toernooiid)
        {

            NTTBGELREEntities ctx = new NTTBGELREEntities();
            System.Data.Common.DbConnection dbConn = ctx.Database.Connection;
            System.Data.SqlClient.SqlConnection sc = (System.Data.SqlClient.SqlConnection)dbConn;
            string adoConnStr = sc.ConnectionString;

            using (SqlConnection connection = new SqlConnection(adoConnStr))
            {
                connection.Open();
                // Stored procedure naam = spSchemasInstellen
                SqlCommand cmd = new SqlCommand("spWedstrijdenInplannen", connection);
                cmd.CommandType = CommandType.StoredProcedure;

                // Paramaters voor de stored procedure
                cmd.Parameters.AddWithValue("@ToernooiID", toernooiid);

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException e)
                {
                    string errorMessage = e.Message;
                    int errorCode = e.ErrorCode;
                    return errorMessage;
                }

            }
            return "De wedstrijden zijn ingepland";
=======
        {
            //var toernooischema = (from ts in db.Toernooischema
            //                      join d in db.Deelnemer on new {ts.ToernooiID, ts.CategorieID, ts.SchemaDeel } equals new { d.ToernooiID, d.CategorieID, d.SchemaDeel}
            //                      join c in db.Categorie on ts.CategorieID equals c.CategorieID
            //                      where ts.SchemaDeel.Equals(id)
            //                      group c by ts into g
            //                      let 
            //                      select ts);
            //                      select new ToernooischemaGegevens { ts = pvv.});

            //var toernooischema = from TS in db.Toernooischema
            //                     join D in db.Deelnemer
            //                           on new { TS.ToernooiID, TS.CategorieID, TS.SchemaDeel }
            //                       equals new { D.ToernooiID, D.CategorieID }
            //                     where
            //                       TS.SchemaDeel == 1 &&
            //                       TS.ToernooiID == id
            //                     group new { TS, TS.Categorie, D } by new
            //                     {
            //                         TS.ToernooiID,
            //                         TS.CategorieID,
            //                         TS.Categorie.Subcategorie,
            //                         TS.Categorie.Geslacht,
            //                         TS.Categorie.EnkelDubbel,
            //                         TS.Categorie.Niveau
            //                     } into g
            //                     select new aap
            //                     {
            //                         ID = g.Key.ToernooiID,
            //                         Geslacht = g.Key.Geslacht,
            //                         Subcategorie = g.Key.Subcategorie,
            //                         EnkelDubbel = g.Key.EnkelDubbel,
            //                         Niveau = g.Key.Niveau,
            //                         Deelnemers = g.Count(p => p.D.SpelerID != null)
            //                     };

            var toernooi = (from t in db.Toernooi
                           where t.ToernooiID == id
                           select t);

            //ViewBag.ToernooiSchema1 = toernooischema.ToList();

            return View(toernooi);
            //return View(toernooischema.ToList());
>>>>>>> 7bc8efd4806e9220f5f9d95b78851a42dcfb1e89
        }


        // GET: Toernooischema/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Toernooischema toernooischema = db.Toernooischema.Find(id);
            if (toernooischema == null)
            {
                return HttpNotFound();
            }
            return View(toernooischema);
        }

        // GET: Toernooischema/Create
        public ActionResult Create()
        {
            ViewBag.CategorieID = new SelectList(db.Categorie, "CategorieID", "Leeftijdsgroep");
            ViewBag.ToernooiID = new SelectList(db.Toernooi, "ToernooiID", "Locatienaam");
            ViewBag.Toernooivorm = new SelectList(db.Toernooivorm, "Toernooivorm1", "Toernooivorm1");
            return View();
        }

        // POST: Toernooischema/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ToernooiID,CategorieID,SchemaDeel,Toernooivorm,AantalPoules,AantalDoor,AantalRondes")] Toernooischema toernooischema)
        {
            if (ModelState.IsValid)
            {
                db.Toernooischema.Add(toernooischema);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CategorieID = new SelectList(db.Categorie, "CategorieID", "Leeftijdsgroep", toernooischema.CategorieID);
            ViewBag.ToernooiID = new SelectList(db.Toernooi, "ToernooiID", "Locatienaam", toernooischema.ToernooiID);
            ViewBag.Toernooivorm = new SelectList(db.Toernooivorm, "Toernooivorm1", "Toernooivorm1", toernooischema.Toernooivorm);
            return View(toernooischema);
        }

        // GET: Toernooischema/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Toernooischema toernooischema = db.Toernooischema.Find(id);
            if (toernooischema == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategorieID = new SelectList(db.Categorie, "CategorieID", "Leeftijdsgroep", toernooischema.CategorieID);
            ViewBag.ToernooiID = new SelectList(db.Toernooi, "ToernooiID", "Locatienaam", toernooischema.ToernooiID);
            ViewBag.Toernooivorm = new SelectList(db.Toernooivorm, "Toernooivorm1", "Toernooivorm1", toernooischema.Toernooivorm);
            return View(toernooischema);
        }

        // POST: Toernooischema/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ToernooiID,CategorieID,SchemaDeel,Toernooivorm,AantalPoules,AantalDoor,AantalRondes")] Toernooischema toernooischema)
        {
            if (ModelState.IsValid)
            {
                db.Entry(toernooischema).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CategorieID = new SelectList(db.Categorie, "CategorieID", "Leeftijdsgroep", toernooischema.CategorieID);
            ViewBag.ToernooiID = new SelectList(db.Toernooi, "ToernooiID", "Locatienaam", toernooischema.ToernooiID);
            ViewBag.Toernooivorm = new SelectList(db.Toernooivorm, "Toernooivorm1", "Toernooivorm1", toernooischema.Toernooivorm);
            return View(toernooischema);
        }

        // GET: Toernooischema/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Toernooischema toernooischema = db.Toernooischema.Find(id);
            if (toernooischema == null)
            {
                return HttpNotFound();
            }
            return View(toernooischema);
        }

        // POST: Toernooischema/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Toernooischema toernooischema = db.Toernooischema.Find(id);
            db.Toernooischema.Remove(toernooischema);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpPost]
        public string SchemaAanpassen(int toernooiID, int categorieID, string toernooivorm1, int? aantalpoules1, int? aantaldoor1, int? aantalrondes1,
                                    string toernooivorm2, int? aantalpoules2, int? aantaldoor2, int? aantalrondes2, string toernooivorm3, int? aantalpoules3, int? aantaldoor3, int? aantalrondes3)
        {
            NTTBGELREEntities ctx = new NTTBGELREEntities();
            System.Data.Common.DbConnection dbConn = ctx.Database.Connection;
            System.Data.SqlClient.SqlConnection sc = (System.Data.SqlClient.SqlConnection)dbConn;
            string adoConnStr = sc.ConnectionString;

            if (toernooivorm2 == "") { toernooivorm2 = null;};
            if (toernooivorm3 == "") { toernooivorm3 = null; };

            using (SqlConnection connection = new SqlConnection(adoConnStr))
            {
                connection.Open();
                // Stored procedure naam = inschrijving
                SqlCommand cmd = new SqlCommand("spSchemaAanpassen", connection);
                cmd.CommandType = CommandType.StoredProcedure;

                // Paramaters voor de stored procedure
                cmd.Parameters.AddWithValue("@ToernooiID", toernooiID);
                cmd.Parameters.AddWithValue("@CategorieID", categorieID);
                cmd.Parameters.AddWithValue("@Toernooivorm1", toernooivorm1);
                cmd.Parameters.AddWithValue("@AantalPoules1", aantalpoules1);
                cmd.Parameters.AddWithValue("@AantalDoor1", aantaldoor1);
                cmd.Parameters.AddWithValue("@AantalRondes1", aantalrondes1);
                cmd.Parameters.AddWithValue("@Toernooivorm2", toernooivorm2);
                cmd.Parameters.AddWithValue("@AantalPoules2", aantalpoules2);
                cmd.Parameters.AddWithValue("@AantalDoor2", aantaldoor2);
                cmd.Parameters.AddWithValue("@AantalRondes2", aantalrondes2);
                cmd.Parameters.AddWithValue("@Toernooivorm3", toernooivorm3);
                cmd.Parameters.AddWithValue("@AantalPoules3", aantalpoules3);
                cmd.Parameters.AddWithValue("@AantalDoor3", aantaldoor3);
                cmd.Parameters.AddWithValue("@AantalRondes3", aantalrondes3);

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException e)
                {
                    string errorMessage = e.Message;
                    int errorCode = e.ErrorCode;
                    return errorMessage;
                }
               
            }
            var aantalwedstrijden = (from Toernooicategorie in db.Toernooicategorie
                                     where
                                       Toernooicategorie.ToernooiID == toernooiID &&
                                       Toernooicategorie.CategorieID == categorieID
                                     select new
                                     {
                                         Toernooicategorie.AantalWedstrijden
                                     }).First();


            var totaalwedstrijden = (from Toernooicategorie in
                                         (from Toernooicategorie in db.Toernooicategorie
                                          where
                                            Toernooicategorie.ToernooiID == toernooiID 
                                          select new
                                          {
                                              Toernooicategorie.AantalWedstrijden,
                                              Dummy = "x"
                                          })
                                     group Toernooicategorie by new { Toernooicategorie.Dummy } into g
                                     select new
                                     {
                                         Aantal = g.Sum(p => p.AantalWedstrijden)
                                     }).First();

            return "Het schema is aangepast._" + toernooiID.ToString() + '_' + totaalwedstrijden.Aantal + "_" + aantalwedstrijden.AantalWedstrijden;
        }

        public Int32 WedstrijdenSchema (int toernooiID, int categorieID)
        {
            NTTBGELREEntities ctx = new NTTBGELREEntities();
                System.Data.Common.DbConnection dbConn = ctx.Database.Connection;
                System.Data.SqlClient.SqlConnection sc = (System.Data.SqlClient.SqlConnection)dbConn;
                string adoConnStr = sc.ConnectionString;

                using (SqlConnection connection = new SqlConnection(adoConnStr))
                {
                    connection.Open();
                    // Stored procedure naam = inschrijving
                    SqlCommand cmd = new SqlCommand("spWedstrijdenSchema", connection);
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    Int32 Aantal;

                    // Paramaters voor de stored procedure
                    cmd.Parameters.AddWithValue("@ToernooiID", toernooiID);
                    cmd.Parameters.AddWithValue("@CategorieID", categorieID);
                    
                    cmd.ExecuteNonQuery();
                    Aantal = (int)returnParameter.Value;

                    return Aantal;
                }

        
       }
    }
}
