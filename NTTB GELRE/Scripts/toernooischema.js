﻿$(document).ready(function () {
    $('select[name^="toernooivormen"]').change();
});

// Selected a toernooivormen
$('select[name^="toernooivormen"]').change(function () {
        var maxToernooivormen = 3;
        var toernooivormnr = 1;
        var selectedid = $(this).attr('id').substr(14);
        var selected = 'toernooivorm' + selectedid;
        var textbox = $(this).attr('id');

        var nr = 1;
        while (nr <= maxToernooivormen) {
            var value = $("#" + selected + '_' + nr).text();
            if (value != null && value != "") {
                toernooivormnr++;
            }
            nr++;
        }
        if ($(this).find('option:selected').text() != "") {
        if (toernooivormnr <= maxToernooivormen) {
            // Place selected value into textfield
            // ID (hidden)
            //$("#" + selected + '_' + toernooivormnr).val($(this).find('option:selected').val());
            // Actual text for the user
            $("#" + selected + '_' + toernooivormnr).text($(this).find('option:selected').text());
            if ($(this).find('option:selected').text() == 'Poule') {
                var aantalpoules1 = 'toernooipoules' + selectedid + '_' + (toernooivormnr);
                var aantalpoules2 = 'aantalpoules' + selectedid + '_' +  (toernooivormnr);
                var aantaldoor1 = 'toernooidoor' + selectedid + '_' + (toernooivormnr);
                var aantaldoor2 = 'aantaldoor' + selectedid + '_' + (toernooivormnr);

                $("#" + aantalpoules1).show();
                $("#" + aantalpoules2).show();
                $("#" + aantaldoor1).show();
                $("#" + aantaldoor2).show();
            };
             if ($(this).find('option:selected').text() == 'Zwitsers') {
                var aantalrondes1 = 'toernooirondes' + selectedid + '_' + (toernooivormnr);
                var aantalrondes2 = 'aantalrondes' + selectedid + '_' + (toernooivormnr);
     
                $("#" + aantalrondes1).show();
                $("#" + aantalrondes2).show();
            };
            // Show/hide delete buttons
            $("#button" + selectedid + '_' + toernooivormnr).show();
            $("#button" + (selectedid + '_' + (toernooivormnr - 1))).hide();
            toernooivormnr++;
            $(this).get(0).selectedIndex = 0;

        }
        else {
            $(this).get(0).selectedIndex = 0;
        }
        }
        deletetoernooivormen(selected + '_' + (toernooivormnr - 1), toernooivormnr - 1, textbox);
   
});

// Delete buttons when pressed
$("input[name^='button']").click(function () {
    $(this).hide();

    var selectedid = $(this).attr('id').substr(6);
    var selected = 'toernooivorm' + selectedid;
    var text = selectedid.substr(0, selectedid.length - 2);
    var text2 = 'toernooivorm'+ text + '_' + (selectedid.substr(selectedid.length - 1) - 1);
    var buttontext = "button" + text + '_' + (selectedid.substr(selectedid.length - 1) - 1);
    var textbox = 'toernooivormen' + text;

    var aantalpoules1 = 'toernooipoules' + text + '_' + (selectedid.substr(selectedid.length - 1));
    var aantalpoules2 = 'aantalpoules' + text + '_' + (selectedid.substr(selectedid.length - 1));
    var aantaldoor1 = 'toernooidoor'+ text + '_' + (selectedid.substr(selectedid.length - 1));
    var aantaldoor2 = 'aantaldoor'+ text + '_' + (selectedid.substr(selectedid.length - 1));
    var aantalrondes1 = 'toernooirondes' + text + '_' + (selectedid.substr(selectedid.length - 1) );
    var aantalrondes2 = 'aantalrondes' + text + '_' + (selectedid.substr(selectedid.length - 1));

    $("#" + aantalpoules1).hide();
    $("#" + aantalpoules2).hide();
    $("#" + aantalpoules2).val(null);
    $("#" + aantaldoor1).hide();
    $("#" + aantaldoor2).hide();
    $("#" + aantaldoor2).val(null);
    $("#" + aantalrondes1).hide();
    $("#" + aantalrondes2).hide();
    $("#" + aantalrondes2).val(null);

    $("#" + selected).text('');
    $("#" + buttontext).show();

    deletetoernooivormen(text2, (selectedid.substr(selectedid.length - 1) - 1), textbox);
});


    // Delete a toernooivormen function
function deletetoernooivormen(text, toernooivormnr, box) {
    
    if (toernooivormnr == 0) {
        $("option:contains('Zwitsers')", "#" + box).removeAttr('disabled');
        $("option:contains('Poule')", "#" + box).removeAttr('disabled');
        $("option:contains('Knock-out')", "#" + box).removeAttr('disabled');
        $("option:contains('Verliezersronde')", "#" + box).attr('disabled', 'disabled');
    }
    else if ($('#' + text).text() == 'Knock-out') {
            $("option:contains('Knock-out')", "#" + box).attr('disabled', 'disabled');
            $("option:contains('Poule')", "#" + box).attr('disabled', 'disabled');
            $("option:contains('Zwitsers')", "#" + box).attr('disabled', 'disabled');
            $("option:contains('Verliezersronde')", "#" + box).attr('disabled', 'disabled');

            if (toernooivormnr == 1) {
                $("option:contains('Verliezersronde')", "#" + box).removeAttr('disabled');
            }
        }

        else if ($('#' + text).text() == "Poule") {
            $("option:contains('Zwitsers')", "#" + box).attr('disabled', 'disabled');
            $("option:contains('Verliezersronde')", "#" + box).attr('disabled', 'disabled');
            $("option:contains('Poule')", "#" + box).removeAttr('disabled');

            if (toernooivormnr == 2) {
                $("option:contains('Poule')", "#" + box).attr('disabled', 'disabled');
                $("option:contains('Verliezersronde')", "#" + box).removeAttr('disabled');
                $("option:contains('Knock-out')", "#" + box).removeAttr('disabled');
           }
        }

        else if ($('#' + text).text() == "Zwitsers") {
            $("option:contains('Poule')", "#" + box).attr('disabled', 'disabled');
            $("option:contains('Knock-out')", "#" + box).attr('disabled', 'disabled');
            $("option:contains('Zwitsers')", "#" + box).attr('disabled', 'disabled');
            $("option:contains('Verliezersronde')", "#" + box).attr('disabled', 'disabled');
        }

        else if ($('#' + text).text() == "Verliezersronde") {
            $("option:contains('Poule')", "#" + box).attr('disabled', 'disabled');
            $("option:contains('Knock-out')", "#" + box).attr('disabled', 'disabled');
            $("option:contains('Zwitsers')", "#" + box).attr('disabled', 'disabled');
            $("option:contains('Verliezersronde')", "#" + box).attr('disabled', 'disabled');
        }
    }

$("input[name^='opslaan']").click(function () {
    var selectedid = $(this).attr('id').substr(7);
    var array = selectedid.split('_');
    var toernooivorm1 = $("#" + 'toernooivorm' + selectedid + '_1').text();

    var toernooiid = array[1];
    var categorieid = array[2];
    var toernooivorm2 = $("#" + 'toernooivorm' + selectedid + '_2').text();
    var toernooivorm3 = $("#" + 'toernooivorm' + selectedid + '_3').text();
    var aantalpoules1 = $("#" + 'aantalpoules' + selectedid + '_1').val();
    var aantaldoor1 = $("#" + 'aantaldoor' + selectedid + '_1').val();
    var aantalrondes1 = $("#" + 'aantalrondes' + selectedid + '_1').val();
    var aantalpoules2 = $("#" + 'aantalpoules' + selectedid + '_2').val();
    var aantaldoor2 = $("#" + 'aantaldoor' + selectedid + '_2').val();
    var aantalrondes2 = $("#" + 'aantalrondes' + selectedid + '_2').val();
    var aantalpoules3 = $("#" + 'aantalpoules' + selectedid + '_3').val();
    var aantaldoor3 = $("#" + 'aantaldoor' + selectedid + '_3').val();
    var aantalrondes3 = $("#" + 'aantalrondes' + selectedid + '_3').val();

    $.ajax({
        url: "/Toernooischema/SchemaAanpassen",
        datatype: "text",
        data: {
            'toernooiID': toernooiid, 'categorieID': categorieid, 'toernooivorm1': toernooivorm1, 'aantalpoules1': aantalpoules1, 'aantaldoor1': aantaldoor1, 'aantalrondes1': aantalrondes1,
            'toernooivorm2': toernooivorm2, 'aantalpoules2': aantalpoules2, 'aantaldoor2': aantaldoor2, 'aantalrondes2': aantalrondes2,
            'toernooivorm3': toernooivorm3, 'aantalpoules3': aantalpoules3, 'aantaldoor3': aantaldoor3, 'aantalrondes3': aantalrondes3},
        type: "POST",
        success: function (data) {
            var aantallen = data.split('_');
            alert(aantallen[0]);
            $('#totaalwedstrijden_' + aantallen[1]).text('Totaal wedstrijden: ' + aantallen[2]);
            $("#aantalwedstrijden"+selectedid).text(aantallen[3] + ' Wedstrijden')
        },
        error: function () {
            alert("ERROR");
        }
    });
});

$("input[name^='instellentoernooivorm']").click(function () {
    var selectedid = $(this).attr('id').substr(22);
    var array = selectedid.split('_');
    var toernooiid = array[0];

    var toernooivorm1 = $("#" + 'toernooivorm_' + toernooiid + '_1').text();
    var toernooivorm2 = $("#" + 'toernooivorm_' + toernooiid + '_2').text();
    var toernooivorm3 = $("#" + 'toernooivorm_' + toernooiid + '_3').text();

    $.ajax({
        url: "/Toernooischema/AlgemeneToernooivorm",
        datatype: "text",
        data: {
            'toernooiID': toernooiid,  'toernooivorm1': toernooivorm1, 'toernooivorm2': toernooivorm2, 'toernooivorm3': toernooivorm3
        },
        type: "POST",
        success: function (data) {
            alert(data);
            window.location.href = toernooiid;
        },
        error: function () {
            alert("ERROR");
        }
    });
});

$("input[name^='instellenstandaard']").click(function () {
    var selectedid = $(this).attr('id').substr(19);
    var array = selectedid.split('_');
    var toernooiid = array[0];

    var poulestandaard = $("#" + 'spelerspoules_' + toernooiid).val();
    var doorstandaard = $("#" + 'spelersdoor_' + toernooiid).val();

    $.ajax({
        url: "/Toernooischema/InstellenStandaard",
        datatype: "text",
        data: {
            'toernooiID': toernooiid, 'poulestandaard': poulestandaard, 'doorstandaard': doorstandaard
        },
        type: "POST",
        success: function (data) {
            alert(data);
            window.location.href = toernooiid;
        },
        error: function () {
            alert("U moet bij beide velden iets invullen!");
        }
    });
});

$("input[name^='wedstrijdeninplannen']").click(function () {
    var selectedid = $(this).attr('id').substr(21);
    var array = selectedid.split('_');
    var toernooiid = array[0];

    $.ajax({
        url: "/Toernooischema/WedstrijdenInplannen",
        datatype: "text",
        data: {
            'toernooiID': toernooiid
        },
        type: "POST",
        success: function (data) {
            alert(data);
            window.location.href = toernooiid;
        },
        error: function () {
            alert("U moet bij beide velden iets invullen!");
        }
    });
});