﻿namespace NTTB_GELRE.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System;
    using System.Linq;
    using System.Web;

    public class Inschrijvingen
    {

        public int SpelerID { get; set; }

        public long ToernooiID { get; set; }

        public int CategorieID { get; set; }

        public string Toernooinaam { get; set; }

        public DateTime Begindatum { get; set; }

        public DateTime Einddatum { get; set; }

        public string EnkelDubbel { get; set; }

        public string Licentie { get; set; }

        public DateTime EindInschrijving { get; set; }

    }

    
}
