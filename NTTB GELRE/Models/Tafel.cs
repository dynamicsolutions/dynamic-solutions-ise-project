//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NTTB_GELRE.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tafel
    {
        public Tafel()
        {
            this.TafelsSchema = new HashSet<TafelsSchema>();
            this.Wedstrijd = new HashSet<Wedstrijd>();
        }
    
        public long ToernooiID { get; set; }
        public int Tafelnummer { get; set; }
    
        public virtual Toernooi Toernooi { get; set; }
        public virtual ICollection<TafelsSchema> TafelsSchema { get; set; }
        public virtual ICollection<Wedstrijd> Wedstrijd { get; set; }
    }
}
