﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using NTTB_GELRE.Models;
using System.Web.Security;

using System.Diagnostics;

namespace NTTB_GELRE.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(Models.UserModel user)
        {
            //bool rememberMe = false;

            if (ModelState.IsValid)
            {
                if (IsValid(user.Email, user.Wachtwoord))
                {
                    FormsAuthentication.SetAuthCookie(user.Email, user.RememberMe);
                    Response.Redirect("../home/index");
                    Session["Werknemer"] = user.Email;
                }
                else
                {
                    ModelState.AddModelError("", "Verkeerde inloggegevens");
                    Session["Rol"] = null;
                    Session["SpelerId"] = null;
                    FormsAuthentication.SignOut();
                }
            }

            return View();
        }

        [HttpGet]
        public ActionResult RegistreerWerknemer()
        {
            var db = new NTTBGELREEntities();
            var rollenList = new List<string>();
            var rollenQry = from m in db.Rollen
                            orderby m.Rol
                            select m.Rol;

            rollenList.AddRange(rollenQry.Distinct());
            ViewBag.rol = new SelectList(rollenList);

            return View();
        }

        [HttpPost]
        public ActionResult RegistreerWerknemer(Models.WerknemerUserModel user)
        {
            var db = new NTTBGELREEntities();

            if (ModelState.IsValid) {
                using (db)
                {
                    if (user.Wachtwoord == user.Herhaal)
                    {
                        var crypto = new SimpleCrypto.PBKDF2();
                        var encrpPass = crypto.Compute(user.Wachtwoord);

                        var sysUser = db.Werknemer.Create();
                        sysUser.Mailadres = user.Email;
                        sysUser.Wachtwoord = encrpPass;
                        sysUser.WachtwoordSalt = crypto.Salt;
                        sysUser.Voornaam = user.Voornaam;
                        sysUser.Tussenvoegsel = user.Tussenvoegsel;
                        sysUser.Achternaam = user.Achternaam;
                        sysUser.Telefoon = user.Telefoon;
                        sysUser.Rol = user.Rol;

                        db.Werknemer.Add(sysUser);
                        db.SaveChanges();

                        return RedirectToAction("Index", "Home");
                    }
                }
            }

            var rollenList = new List<string>();
            var rollenQry = from m in db.Rollen
                            orderby m.Rol
                            select m.Rol;

            rollenList.AddRange(rollenQry.Distinct());
            ViewBag.rollen = new SelectList(rollenList);
            
            return View();
        }

        [HttpGet]
        public ActionResult RegistreerSpeler()
        {
            var geslachtList = new List<string>();
            geslachtList.Add("M");
            geslachtList.Add("V");
            ViewBag.geslacht = new SelectList(geslachtList);

            return View();
        }

        [HttpPost]
        public ActionResult RegistreerSpeler(Models.SpelerUserModel user)
        {
            var db = new NTTBGELREEntities();

            if (ModelState.IsValid)
            {
                using (db)
                {
                    if (user.Wachtwoord == user.Herhaal)
                    {
                        var crypto = new SimpleCrypto.PBKDF2();
                        var encrpPass = crypto.Compute(user.Wachtwoord);

                        var sysUser = db.Speler.Create();
                        sysUser.Mailadres = user.Email;
                        sysUser.Wachtwoord = encrpPass;
                        sysUser.WachtwoordSalt = crypto.Salt;
                        sysUser.Voornaam = user.Voornaam;
                        sysUser.Tussenvoegsel = user.Tussenvoegsel;
                        sysUser.Achternaam = user.Achternaam;
                        sysUser.Telefoonnummer = user.Telefoon;
                        sysUser.Geslacht = user.Geslacht;
                        sysUser.Telefoonnummer = user.Telefoon;
                        sysUser.Geboortedatum = user.Geboortedatum;
                        sysUser.Bondsnummer = user.Bondsnummer;
                        sysUser.Licentie = user.Licentie;
                        sysUser.Punten = user.Punten;

                        db.Speler.Add(sysUser);
                        db.SaveChanges();

                        return RedirectToAction("Index", "Home");
                    }
                }
            }

            var geslachtList = new List<string>();
            geslachtList.Add("Man");
            geslachtList.Add("Vrouw");
            ViewBag.geslachten = new SelectList(geslachtList);

            return View();
        }

        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            return RedirectToAction("Index", "Home");
        }

        private bool IsValid(String email, String password)
        {
            var crypto = new SimpleCrypto.PBKDF2();
            bool isValid = false;

            using (var db = new NTTBGELREEntities())
            {
                var userWerknemer = db.Werknemer.FirstOrDefault(u => u.Mailadres == email);
                if (userWerknemer != null)
                {
                    if (userWerknemer.Wachtwoord == crypto.Compute(password, userWerknemer.WachtwoordSalt))
                    {
                        isValid = true;
                    }
                }
                if (!isValid)
                {
                    var userSpeler = db.Speler.FirstOrDefault(u => u.Mailadres == email);
                    if (userSpeler != null)
                    {
                        if (userSpeler.Wachtwoord == crypto.Compute(password, userSpeler.WachtwoordSalt))
                        {
                            isValid = true;
                        }
                    }
                }
            }

            return isValid;       
        }
    }
}