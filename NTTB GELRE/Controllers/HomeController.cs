﻿using NTTB_GELRE.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NTTB_GELRE.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (User.Identity.Name != "")
            {
                var db = new NTTBGELREEntities();
                var rollenQry = from m in db.Werknemer
                                where m.Mailadres == User.Identity.Name
                                select m.Rol;
                if (rollenQry.Count() != 0)
                {
                    Session["Rol"] = rollenQry.First();
                    Session["SpelerId"] = null;
                }
                else
                {
                    var spelerIdQry = from m in db.Speler
                                    where m.Mailadres == User.Identity.Name
                                    select m.SpelerID;

                    if (spelerIdQry.Count() != 0)
                    {
                        Session["SpelerId"] = spelerIdQry.First();
                    }
                    Session["Rol"] = "Speler";
                }

                
            }
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}